import 'package:facebook_audience_network/facebook_audience_network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:jathakakatha/data/constants.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  String version = "";
  String packageName = "";

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = "${packageInfo.version}.${packageInfo.buildNumber}";
        packageName = packageInfo.packageName;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    FacebookInterstitialAd.loadInterstitialAd(
      placementId: GlobalConfiguration().getString("AboutScreenFull"),
      listener: (result, value) {
        if (result == InterstitialAdResult.LOADED)
          FacebookInterstitialAd.showInterstitialAd(delay: constAdsAboutScreen);
      },
    );

    return Scaffold(
      backgroundColor: constColorPrimary,
      appBar: AppBar(
        elevation: 0,
        bottomOpacity: 1.0,
        backgroundColor: constColorPrimary,
        title: Text(
          "$constAbout",
          textDirection: TextDirection.ltr,
          style: TextStyle(
              fontSize: constFontSizeTitle, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [constColorPrimary, constColorDefaultText],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0.1, 1])),
          child: MediaQuery.of(context).orientation == Orientation.portrait
              ? Column(
                  children: <Widget>[
                    Container(
                      height: 100,
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          alignment: Alignment(0, 0.7),
                          image: constImageAboutBanner,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    getAboutInfo()
                  ],
                )
              : Row(
                  children: <Widget>[
                    Container(
                      width: 200,
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          alignment: Alignment(-0.3, -0.3),
                          image: constImageAboutBanner,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    getAboutInfo()
                  ],
                )),
    );
  }

  Widget getAboutInfo() {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: constPaddingSpace),
        child: ListView(
          children: <Widget>[
            Container(height: constPaddingSpace),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      " $constPansiyaPanas $constAppName",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: constFontSizeTileIndex,
                          shadows: constDefaultTextShadow,
                          color: constColorDefaultText),
                    ),
                    Text(
                      "$version $constStringEdition",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: constFontSizeBody,
                      ),
                    ),
                  ],
                ),
                Spacer(flex: 1),
                Column(
                  children: <Widget>[
                    InkWell(
                        child: Image(
                          image: constImageGooglePlay,
                          width: 180,
                        ),
                        onTap: () => _launchURL("$constStringAppUrl$packageName")),
                    Text(
                      constStringRateUs,
                      style: TextStyle(
                          fontSize: constFontSizeStory,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                )
              ],
            ),
            Container(
              height: constPaddingSpace,
            ),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      constStringTextsSourceLabel,
                      style: TextStyle(fontSize: constFontSizeBody),
                    ),
                    Expanded(
                      child: Padding(
                        padding:
                            EdgeInsets.fromLTRB(constPaddingSpace, 0, 0, 0),
                        child: InkWell(
                          child: Text(
                            constStringTextsSource,
                            style: TextStyle(
                                fontSize: constFontSizeBody,
                                color: Colors.white,
                                shadows: constDefaultTextShadow),
                            maxLines: 2,
                          ),
                          onTap: () => _launchURL(constStringSourceUrl2),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
            Container(
              height: constPaddingSpace,
            ),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      constStringImagesSourceLabel,
                      style: TextStyle(fontSize: constFontSizeBody),
                    ),
                    Expanded(
                      child: Padding(
                        padding:
                            EdgeInsets.fromLTRB(constPaddingSpace, 0, 0, 0),
                        child: InkWell(
                          child: Text(
                            constStringImagesSource,
                            style: TextStyle(
                                fontSize: constFontSizeBody,
                                color: constColorLink,
                                shadows: constDefaultTextShadow),
                            maxLines: 2,
                          ),
                          onTap: () => _launchURL(constStringSourceUrl1),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
            Container(
              height: constPaddingSpace * 2,
            ),
            Text(
              constStringAboutBook,
              style: TextStyle(
                  color: constColorDefaultText,
                  fontSize: constFontSizeTileTitle,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[constShadowTextUpRight]),
            ),
            Text(
              constAboutBook,
              style: TextStyle(fontSize: constFontSizeBody),
              textAlign: TextAlign.justify,
            )
          ],
        ),
      ),
    );
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
