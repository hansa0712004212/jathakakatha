import 'package:flutter/material.dart';

// Strings
final String constPansiyaPanas = "550";
final String constAppName = "ජාතක කතා";
final String constSearch = "සොයන්න";
final String constExit = "පිටවීම";
final String constYes = "ඔව්";
final String constNo = "නැත";
final String constExitConfirmMessage =
    "$constPansiyaPanas $constAppName තුලින් ඉවත්වීමට අවශ්‍යද ?";
final String constTtsLanguage = "si-LK";
final int constTtsMaxLength = 3999;
final double constShadowTextBlurRadius = 2;
final String constAbout = "යෙදවුම පිලිබඳ";
final String constAboutBook = '"ජාතං - භූතං - පුරාවුත්තං භගවතො පුබ්බ චරිතං කායති කථේති පකාඝෝ තී ති ජාතකං"\n\n"ජාතක සංඛ්‍යාත පුරාවෘත්තය හෙවත් භාග්‍යවතුන් වහන්සේ පෙර බෝසත් කල්හී ඒ ඒ ඡුතීන්හී පැවති ආශ්චර්ය චරිත විශේෂය ප‍්‍රකාශ කරන්නා වූ ශ‍්‍රී මුඛ දේශනා ජාතක නම් වේ"\n\nබුදුරජාණන් වහන්සේ යම් යම් කරුණු පැහැදිලි කරදීම පිණිස සිය ධර්ම දේශනාවලදී තමන් වහන්සේගේ පෙර බෝසත් චරිතයන් ඇසුරු කොටගෙන ඇති ජාතක කථා දේශනා කර ඇත. ත‍්‍රිපිටකයට අයත් සූත‍්‍ර පිටකයෙහි එන එක්තරා ග‍්‍රන්ථයක් ලෙස ජාතක පාළි නම් ග‍්‍රන්ථය හැඳින්විය හැකිය. ජාතක පාළිය වනාහි පාලි පද්‍යයෙන් රචනා කර ඇති ග‍්‍රන්ථයකි. එහි සම්පූර්ණ කථාව දක්වා නැත. සැබවින්ම කියතොත් ජාතක පාළිය බලා ජාතක කථා තේරුම් ගත නොහැක. එහි දක්වා ඇත්තේ ජාතක කථාවල සාරය පමණි. මෙකී ජාතක පාළි නමැති ග‍්‍රන්ථයට ක‍්‍රි.ව. 5 වන සියවසේදී දඹදිවින් ලක්දිවට වැඩම කළ බුද්ධඝෝෂ අටුවාචාරින් වහන්සේ විසින් රචනා කරන ලද ජාතකට්ඨ කථාව සම්පූර්ණ ජාතක කථා දක්වන පාලි අටුවා ග‍්‍රන්ථයකි. මෙකී පාලි ජාතක කට්ඨාකථාව ඉතාම සමීපයෙන් ඇසුරු කරමින් සිංහල ජාතක පොත රචනා කර ඇත. \n\nජාතක සාහිත්‍යය ඈත අතීතයේ පටන්ම ඉන්දීය ජනතාව අතර ගැමි කථා වශයෙන් පැවතී ඇත. පසු කාලීනව බුද්ධ දේශනා වශයෙන් ප‍්‍රචලිත වී පැමිණි මෙම ජාතක කථා ලොව පුරා රටවල් විශාල ප‍්‍රමාණයක සාහිත්‍යය පෝෂණය කිරීමට සමත්වී ඇත. සිංහල ජාතක පොත රචනා වීමටත් පෙර සිටම සිංහල ජනතාව අතර ජාතක කථා ප‍්‍රචලිත වී තිබූ බවට සාක්‍ෂි රැසක් හමුවේ.\n\n• ක‍්‍රි.ව. දොළොස්වන සියවසේදී පමණ ජාතක අටුවා ගැටපදය නමැති ග‍්‍රන්ථය සිංහල භාෂාවෙන් රචනා වීම.\n• ජාතක ගාථා සන්නය, වෙසතුරාදා සන්නය, දස ජාතක සන්නය, අටදා සන්නය, වැනි සන්න පොත් සිංහලයෙන් රචනා වීම.\n• අමාවතුර, බුත්සරණ, ධර්මප‍්‍රදීපිකාව වැනි ග‍්‍රන්ථ වල ජාතක කථාවන්ගෙන් සාරය හා ජාතක කථා ගෙනහැර දැක්වීම.\n• සස, මඛාදේව, කුස යන ජාතක කථා වස්තු විෂය කොටගෙන පිළිවෙලින් සසදාවත, මුවදෙව්දාවත, කව් සිළුමිණ යන පද්‍ය ග‍්‍රන්ථ තුන රචනාවීම.\n• උම්මංග්ග ජාතකය වෙනම ග‍්‍රන්ථයක් ලෙස රචනාවීම.\n• සිතුවම් නිර්මාණයේදී ජාතක කථා වස්තු විෂය කොට ගැනීම ජාතක ප‍්‍රමාණය සුමංගල විලාසිනිසමන්ත, පාසාදිකා යන අටුවා ග‍්‍රන්ථ වල "පඤ්ඤාසාධිකානි පඤ්ච ජාතක සතානි" යනුවෙන් දක්වන පරිදි ජාතක කථා පන්සිය පනහක් ඇතිබව පැහැදිලි වෙයි.\n\nනමුත් සිංහල ජාතක පොතෙහි දක්නට ලැබෙන්නේ ජාතක කතා 547 ක් පමණි. ආසන්න ගණන සැලකිල්ලට ගෙන මෙම කථා සංග‍්‍රහය "පන්සිය පනස් ජාතක පොත" නමින් හඳුන්වන්නට ඇත.\n\nඅරමුණ අතිශ්‍රේෂ්ඨ චරිතයක් වන බෝසත් චරිතය ඇසුරින් ජනතාවට යහපත් උපදෙස් ලබාදීම ප‍්‍රධාන අරමුණ කොටගෙන ජාතක පොත රචනා කර ඇති බව පැහැදිලි වේ. කොටින්ම කිවහොත් ජාතක පොත යනු උපදේශ ආකරයකි. බෝධිසත්වයන් නිතරම ගතකර ඇත්තේ ආදර්ශවත් ජීවිතයකි. මිනිසුන් පමණක් නොව සතුන් පවා උපයෝගී කොටගෙන ජාතක කථාකරු නිරතුරුවම උත්සහකර ඇත්තේ පාඨක / ශ‍්‍රාවක සහෘදයන්ට සදුපදේශ ප‍්‍රදානය කිරීමටයි. ජාතක පොතටම ආවේණික වූ ආකෘතියක් ඇත.\n\nසියලූම ජාතක කථා වලට පොදු වූ එම ආකෘතිය කොටස් තුනකින් සකස් වී ඇත.\n\n• වර්ථමාන කථාව ජාතක කථා ආකෘතියෙහි ප‍්‍රථම කොටස වන වර්තමාන කථාව බුද්ධ කාලය හා සම්බන්‍ධය. බොහෝවිට බුදුරජාණන් වහන්සේ ධර්ම සභාවට වඩින විට උන්වහන්සේට පෙරාතුව එහි පැමිණ සිටින භික්‍ෂුන් වහන්සේලා විසින් කතාබහ කරමින් සිටි කිසියම් පුද්ගලයෙක් හෝ සිද්ධියක් පිළිබඳව වර්තමාන කථාවේදී කියවෙයි.\n• අතීත කථාව ”මා ධර්ම සභාවට වැඩම කිරිමට පෙරාතුව ඔබ වහන්සේලා කිනම් කතාවකින් යුක්තව සිටියෝ දැයි අසන බුදුහිමියෝ භික්‍ෂුන්වහන්සේලාගෙන් තොරතුරු දැනගෙන ”මහණෙනි දැන් පමණක් නොවෙයි අතීතයේදීත් එම පුද්ගලයා එලෙස කටයුතු කලා” යනුවෙන් බොහෝ අවස්ථාවලදි දේශනා කරති. ”ස්වාමීනි එම අතීත සිදුවීම දේශනා කරන්න” යනුවෙන් ඉල්ලීම් කළ පසු බුදු හිමියෝ අතීත කථාව වදාරති. සෑම අතීත කථාවකම පාහේ ප‍්‍රධාන භූමිකාව වන්නේ බෝසත් චරිතයයි.\n• පූර්වාපර සන්‍ධි ගැලපීම වර්තමාන කථාවෙහි හා අතීත කථාවෙහි එන චරිත සසඳමින් සමෝධානය සිදුකරන අවස්ථාව පූර්වාපර සන්‍ධි ගැලපීම නමින් හඳුන්වයි.\n';
final String constStringRateUs = "අගයන්න";
final String constStringEdition = "සංස්කරණය";
final String constStringAppUrl = "https://play.google.com/store/apps/details?id=";
final String constStringSourceUrl1 = "https://www.sjp.ac.lk/buddhism/download-buddhist-555-jathaka-katha-free-pdf/";
final String constStringSourceUrl2 = "http://jathakakatha.lk";
final String constStringTextsSourceLabel = "ජාතක කතා\nඋපුටා ගැනීම";
final String constStringTextsSource = "ශ්‍රී ජයවර්ධනපුර විශ්ව විද්‍යාල වෙබ් අඩවියෙන්";
final String constStringImagesSourceLabel = "රූප\nඋපුටා ගැනීම";
final String constStringImagesSource = "jathakakatha.lk වෙබ් අඩවියෙන්";
final String constStringAboutBook = " ජාතක කතා පොත ගැන";

// Colors
final Color constColorPrimary = Colors.amber;
final Color constColorSecondary = Colors.orange;
final Color constColorSplashNumberTitle = Color.fromARGB(255, 255, 191, 0);
final Color constColorSplashTextTitle = Color.fromARGB(255, 255, 0, 127);
final Color constColorDefaultText = Colors.white;
final Color constColorDefaultFill = Colors.white;
final Color constColorDefaultDisabled = Colors.grey;
final Color constColorIcon = Colors.white;
final Color constColorIconShadow = Colors.black;
final Color constColorTextShadow = Colors.black;
final Color constColorIconSplash = Colors.orange;
final Color constColorTransparent = Colors.transparent;
final Color constColorLink = Colors.lightBlueAccent;

// Images
final AssetImage constWelcomeBackgroundImage =
    AssetImage("assets/lord-buddha.jpg");
final AssetImage constImageAppIcon = AssetImage("assets/buddha_baby.png");
final AssetImage constImageAboutBanner = AssetImage("assets/about.jpg");
final AssetImage constImageGooglePlay = AssetImage("assets/googleplay.png");

// Font Sizes
final double constFontSizeTitle = 24;
final double constFontSizeTileIndex = 24;
final double constFontSizeTileTitle = 20;
final double constFontSizeStory = 16;
final double constFontSizeBody = 16;
const double constFontSizeStoryMax = 28;
const double constFontSizeStoryMin = 12;

// Border Sizes
final double constBorderRadius = 10;
final double constBorderWidth = 0.8;
final double constTextContentPadding = 10;
final double constIconSize = 20;
final double constPaddingSpace = 10;

// Lists, Arrays
List<Color> constColorsColourfulProgressBar = [
  Color.fromARGB(255, 0, 0, 255),
  Colors.yellow,
  Colors.red,
  Colors.white,
  Colors.orange
];

Map<String, double> fontSizeMap = <String, double>{
  "ඉතා සෙමෙන්": 0.6,
  "සෙමෙන්": 0.8,
  "සාමාන්‍ය": 1,
  "වේගයෙන්": 1.2,
  "ඉතා වේගයෙන්": 1.4
};

// Shadows
final Shadow constShadowTextUpRight = Shadow(
  offset: Offset(1.0, 1.5),
  blurRadius: constShadowTextBlurRadius,
  color: constColorTextShadow,
);
final Shadow constShadowTextDownRight = Shadow(
  offset: Offset(1.0, -1.5),
  blurRadius: constShadowTextBlurRadius,
  color: constColorTextShadow,
);
final Shadow constShadowTextDownLeft = Shadow(
  offset: Offset(-1.0, -1.5),
  blurRadius: constShadowTextBlurRadius,
  color: constColorTextShadow,
);
final Shadow constShadowTextUpLeft = Shadow(
  offset: Offset(-1.0, 1.5),
  blurRadius: constShadowTextBlurRadius,
  color: constColorTextShadow,
);
final List<Shadow> constDefaultTextShadow = <Shadow>[
  constShadowTextDownLeft,
  constShadowTextDownRight,
  constShadowTextUpRight,
  constShadowTextUpLeft
];

// Ad timer intervals
final int constAdsHomeScreen = 2000;
final int constAdsAboutScreen = 2000;